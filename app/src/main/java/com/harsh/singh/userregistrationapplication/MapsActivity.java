package com.harsh.singh.userregistrationapplication;

import android.*;
import android.Manifest;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int SELECT_PHOTO = 1999;
    private static final int CAMERA_REQUEST = 1888;
    private GoogleMap mMap;
    private ImageView mUploadImage;
    private LocationManager mLocationManager;
    private Location mLocation;
    private EditText mLatLocation_Et;
    private EditText mName_Et;
    private EditText mDateofBirth_Et;
    private EditText mEmail_Et;
    private EditText mPhone_Et;
    private EditText mAddress_Et;
    private MyHelper mMyHelper;
    private SQLiteDatabase mDb;
    private Cursor cursor;
    private Bitmap src;
    private EditText mLngLocation_Et;
    private String LocationStr;
    private String ImgPath;
    private int Age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mName_Et = (EditText) findViewById(R.id.name_et);
        mDateofBirth_Et = (EditText) findViewById(R.id.dateofbirth_et);
        mEmail_Et = (EditText) findViewById(R.id.email_et);
        mPhone_Et = (EditText) findViewById(R.id.phone_et);
        mAddress_Et = (EditText) findViewById(R.id.address_et);
        mUploadImage = (ImageView) findViewById(R.id.UploadimageView);
        mLatLocation_Et = (EditText) findViewById(R.id.latlocation_et);
        mLngLocation_Et = (EditText) findViewById(R.id.lnglocation_et);
        src=BitmapFactory.decodeResource(getResources(), R.id.UploadimageView);

        mMyHelper = new MyHelper(MapsActivity.this, "USERDB", null,1);
        mDb = mMyHelper.getWritableDatabase();


        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, 56);

            return;
        } else {
            mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (mLocation == null) {
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (mLocation == null) {
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 56) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(mLocation==null)
                {
                    mLocation=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                if(mLocation==null)
                {
                    mLocation=mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
                // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this); //this will create the sync task(Saperate thread) to make the request for map
            }
        }
    }


    public void UploadPic(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Source");

        builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_PHOTO);
            }
        });

        builder.setNeutralButton("Camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent= new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, CAMERA_REQUEST);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST) {

            Bitmap photo = (Bitmap) data.getExtras().get("data");
            mUploadImage.setImageBitmap(photo);

            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = managedQuery(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection, null, null, null);
            int column_index_data = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToLast();

            ImgPath=cursor.getString(column_index_data);

        }

        else if(requestCode == SELECT_PHOTO)
        {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                mUploadImage.setImageBitmap(bitmap);

                ImgPath=uri.toString();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng UserLocation = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        mMap.addMarker(new MarkerOptions().position(UserLocation).title("User Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UserLocation,21));
        LocationStr = UserLocation.toString();
        String [] SplitStr = LocationStr.split(",");

        String str= SplitStr[0];
        StringBuilder sb = new StringBuilder(str);
        sb.delete(0,10);
        String LatLoc = sb.toString();
        Double LntValue = Double.valueOf(LatLoc);
        mLatLocation_Et.setText(LntValue.toString());

        String LangLoc = SplitStr[1].replace(")","");
        Double LngValue = Double.valueOf(LangLoc);
        mLngLocation_Et.setText(LngValue.toString());
    }


    public void SaveData(View view) {

        StringBuilder Agesb = new StringBuilder(mDateofBirth_Et.getText().toString());
        Agesb.delete(4,10);
        String Agevalue = Agesb.toString();
        int OrigAge = Integer.valueOf(Agevalue);
        Age = 2017-OrigAge;

        ContentValues cv = new ContentValues();
        cv.put("user_name", mName_Et.getText().toString());
        cv.put("user_dof", mDateofBirth_Et.getText().toString());
        cv.put("user_email", mEmail_Et.getText().toString());
        cv.put("user_phone", Integer.valueOf(mPhone_Et.getText().toString()));
        cv.put("user_address", mAddress_Et.getText().toString());
        cv.put("user_Latlocation" , mLatLocation_Et.getText().toString());
        cv.put("user_Lnglocation" , mLngLocation_Et.getText().toString());
        cv.put("user_image", ImgPath.toString());
        cv.put("user_age" , Age);
        long id = mDb.insert("userinfo",null ,cv);
        Toast.makeText(MapsActivity.this, "User registered Successfully : "+id+" "+Age, Toast.LENGTH_LONG).show();

    }

    public void readData(View view) {

        Intent intent = new Intent(MapsActivity.this, ViewActivity.class);
        startActivity(intent);

        /*if(cursor.moveToNext())
        {
            String name = cursor.getString(1);
            mName_Et.setText(name);

            String dof = cursor.getString(2);
            mDateofBirth_Et.setText(dof);

            String email = cursor.getString(3);
            mEmail_Et.setText(email);

            String phone = cursor.getString(4);
            mPhone_Et.setText(phone);

            String address = cursor.getString(5);
            mAddress_Et.setText(address);

            String Latlocation = cursor.getString(6);
            mLatLocation_Et.setText(Latlocation);

            String Lnglocation = cursor.getString(7);
            mLngLocation_Et.setText(Lnglocation);

            String s = cursor.getString(8);
            mUploadImage.setImageURI(Uri.parse(s));

            String Age1 = cursor.getString(9);
            Toast.makeText(MapsActivity.this, "Age : "+Age1, Toast.LENGTH_SHORT).show();

        }*/
    }
}
