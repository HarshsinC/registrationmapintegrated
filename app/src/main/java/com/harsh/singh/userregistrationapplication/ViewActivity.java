package com.harsh.singh.userregistrationapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ViewActivity extends AppCompatActivity {


    private MyHelper mMyHelper;
    private SQLiteDatabase mDb;
    private Cursor cursor;
    private MyAdapter myAdapter;
    private ListView List_item;
    private String mName_search;
    private String mPhone_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        List_item = (ListView) findViewById(R.id.ListView);

        mMyHelper = new MyHelper(ViewActivity.this, "USERDB", null,1);
        mDb = mMyHelper.getWritableDatabase();
        cursor = mDb.query("userinfo",null, null, null, null, null, null);

        myAdapter = new MyAdapter(this,cursor);
        List_item.setAdapter(myAdapter);
    }

    public void Filter_list(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Age Group");

        builder.setPositiveButton("10-20 Age", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                cursor = mDb.rawQuery("SELECT * FROM userinfo WHERE user_age BETWEEN 10 AND 20", null);
                myAdapter = new MyAdapter(ViewActivity.this,cursor);
                ListView AgeFilter = (ListView) findViewById(R.id.ListView);
                AgeFilter.setAdapter(myAdapter);

            }
        });



        builder.setNeutralButton("20-30 Age", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                cursor = mDb.rawQuery("SELECT * FROM userinfo WHERE user_age BETWEEN 20 AND 30", null);
                myAdapter = new MyAdapter(ViewActivity.this,cursor);
                ListView AgeFilter = (ListView) findViewById(R.id.ListView);
                AgeFilter.setAdapter(myAdapter);

            }
        });

        builder.setNegativeButton("View All", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                cursor = mDb.rawQuery("SELECT * FROM userinfo", null);
                myAdapter = new MyAdapter(ViewActivity.this,cursor);
                ListView AgeFilter = (ListView) findViewById(R.id.ListView);
                AgeFilter.setAdapter(myAdapter);

            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }



    public void Search_List(View view) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Search User");

        final EditText Name_search = new EditText(ViewActivity.this);
        final EditText Phone_search = new EditText(ViewActivity.this);


        LinearLayout ll=new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(Name_search);
        ll.addView(Phone_search);
        alert.setView(ll);

        Name_search.setHint("Name");
        Phone_search.setHint("Mobile");

        alert.setPositiveButton("Search", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                mName_search = Name_search.getText().toString();
                mPhone_search = Phone_search.getText().toString();

                cursor=mDb.rawQuery("SELECT * FROM userinfo WHERE user_name=? AND user_phone=?",new String[]{mName_search,mPhone_search});
                myAdapter = new MyAdapter(ViewActivity.this,cursor);
                ListView AgeFilter = (ListView) findViewById(R.id.ListView);
                AgeFilter.setAdapter(myAdapter);

            }
        });
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();

    }

}

