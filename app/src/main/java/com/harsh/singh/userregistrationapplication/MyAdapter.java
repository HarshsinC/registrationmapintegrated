package com.harsh.singh.userregistrationapplication;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Harsh on 05/02/2017.
 */

public class MyAdapter extends CursorAdapter {

    private Cursor cursor;
    private TextView mListname_tv;
    private TextView mListDOB_tv;
    private TextView mListEmail_tv;
    private TextView mListPhone_tv;
    private TextView mListAdd_tv;
    private TextView mListLat_tv;
    private TextView mListLng_tv;
    private ImageView mListImage_tv;


    public MyAdapter(Context context, Cursor c) {
        super(context, c);
        cursor=c;
    }



    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        mListname_tv = (TextView) view.findViewById(R.id.Listname_tv);
        String name = cursor.getString(cursor.getColumnIndexOrThrow("user_name"));
        mListname_tv.setText(name);

        mListDOB_tv = (TextView) view.findViewById(R.id.ListDOB_tv);
        String DOB = cursor.getString(cursor.getColumnIndexOrThrow("user_dof"));
        mListDOB_tv.setText(DOB);

        mListEmail_tv = (TextView) view.findViewById(R.id.ListEmail_tv);
        String email = cursor.getString(cursor.getColumnIndexOrThrow("user_email"));
        mListEmail_tv.setText(email);

        mListPhone_tv = (TextView) view.findViewById(R.id.ListPhone_tv);
        String phone = cursor.getString(cursor.getColumnIndexOrThrow("user_phone"));
        mListPhone_tv.setText(phone);

        mListAdd_tv = (TextView) view.findViewById(R.id.ListAdd_tv);
        String Add = cursor.getString(cursor.getColumnIndexOrThrow("user_address"));
        mListAdd_tv.setText(Add);

        mListLat_tv = (TextView) view.findViewById(R.id.ListLat_tv);
        String Lat = cursor.getString(cursor.getColumnIndexOrThrow("user_Latlocation"));
        mListLat_tv.setText(Lat);

        mListLng_tv = (TextView) view.findViewById(R.id.ListLng_tv);
        String Lng = cursor.getString(cursor.getColumnIndexOrThrow("user_Lnglocation"));
        mListLng_tv.setText(Lng);

        mListImage_tv = (ImageView) view.findViewById(R.id.ListimageView);
        String Img = cursor.getString(cursor.getColumnIndexOrThrow("user_image"));
        mListImage_tv.setImageURI(Uri.parse(Img));
    }
}
